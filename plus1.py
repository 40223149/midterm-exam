#@+leo-ver=5-thin
#@+node:ppython.20131221094631.1649: * @file diamond.py
#coding: utf-8
# c1w7_1.py, 這個程式示範如何將菱形列印程式由單機搬到 OpenShift


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:ppython.20131221094631.1650: ** <<declarations>> (diamond)
import os
import cherrypy
#@-<<declarations>>
#@+others
#@+node:ppython.20131221094631.1651: ** 列印星號
# 這個自訂函式呼叫時以輸入變數控制後續的列印內容
class Plus(object):
    """docstring
    從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
    """
    # 表單有中文要加上這個
    _cp_config = {
        # 配合 utf-8 格式之表單內容
        # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
        'tools.encode.encoding': 'utf-8'
    }
    def index(self):
        '''
        這裡是首頁喔!
        '''
        Outstring = '''
        <h1>plus</h1>
        <h2>從start的值累加到stop的值, 總數為何</h2>
        <form method="post" action="doAct">
        start:<input type=text name=a ><br />
        stop:<input type=text name=b ><br />
        <input type="submit" value="send">
        <input type="reset" value="reset">
        </form>
        '''
        return Outstring
    index.exposed = True
    def doAct(self, a=None, b=None):
        '''
        while True:
            if int(a)>0 and int(b)>0 and int(a)<int(b):
                break
            else:
                return"error"
        '''
        x = 0
        if(a and b):
            try:
                a = int(a)
                b = int(b)
            except:
                raise cherrypy.HPlusPRedirect("/")
            if a>b:
                a, b = int(b), int(a)
            #輸入變數=51
            數列1 = list(range(int(a),int(b)))
            x=0
            for i in 數列1:
                x=x+i
            return "總和:" + str(x)
    doAct.exposed = True
root = Plus()
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(Plus())
    else:
        # 近端執行啟動
        
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        
        cherrypy.quickstart(Plus())
#@-leo
